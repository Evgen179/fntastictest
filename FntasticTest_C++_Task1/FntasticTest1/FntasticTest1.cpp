// FntasticTest1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

using namespace std;

int main()
{
    string str;
    cout << "Type your text: ";
    getline(cin, str);

    for (int i = 0; i < str.length(); i++)
    {
        int duplicatesFound = 0;
        for (int j = 0; j < str.length(); j++)
        {
            if (tolower(str[i]) == tolower(str[j]))
            {
                duplicatesFound++;
            }
        }

        if (duplicatesFound > 1)
        {
            cout << ")";
        }
        else
        {
            cout << "(";
        }
    }

    return 0;
}

